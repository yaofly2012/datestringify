const dateStringify = require('../src/index');

describe('dateStringify', () => {
    describe('local time zone', () => {
        var source = new Date('2020-05-01');
        test('Test yyyy', () => {    
            const actual = dateStringify('yyyy', source);
            expect(actual).toBe('2020');
        })

        test('Test YYYY', () => {    
            const actual = dateStringify('yyyy', source);
            expect(actual).toBe('2020');
        })

        test('Test yyy', () => {    
            const actual = dateStringify('yyy', source, true);            
            expect(actual).toBe('020');
        })

        test('Test YYY', () => {    
            const actual = dateStringify('YYY', source, true);            
            expect(actual).toBe('020');
        })

        test('Test yy', () => {    
            const actual = dateStringify('yy', source, true);            
            expect(actual).toBe('20');
        })

        test('Test YY', () => {    
            const actual = dateStringify('yy', source, true);            
            expect(actual).toBe('20');
        })

        test('Test MM', () => {    
            const actual = dateStringify('MM', source);            
            expect(actual).toBe('05');
        })

        test('Test M', () => {    
            const actual = dateStringify('M', source);        
            expect(actual).toBe('5');
        })

        test('Test DD', () => {    
            const actual = dateStringify('DD', source);            
            expect(actual).toBe('01');
        })

        test('Test dd', () => {    
            const actual = dateStringify('dd', source);            
            expect(actual).toBe('01');
        })

        test('Test D', () => {    
            const actual = dateStringify('D', source);
            expect(actual).toBe('1');
        })

        test('Test d', () => {    
            const actual = dateStringify('d', source);
            expect(actual).toBe('1');
        })

        test('Test yyyy-MM', () => {    
            const actual = dateStringify('yyyy-MM', source);            
            expect(actual).toBe('2020-05');
        })

        test('Test yyyy-MM-dd', () => {    
            const actual = dateStringify('yyyy-MM-dd', source);            
            expect(actual).toBe('2020-05-01');
        })

        test('Test yyyy-M-d', () => {    
            const actual = dateStringify('yyyy-M-d', source);            
            expect(actual).toBe('2020-5-1');
        })

        test('Test yyyy-MM-dd & yyyy/MM/dd', () => {    
            const actual = dateStringify('yyyy-MM-dd & yyyy/MM/dd', source);            
            expect(actual).toBe('2020-05-01 & 2020/05/01');
        })

        test('Test hh:mm:ss', () => {    
            const source = new Date('2020/05/11 13:14:15');
            const actual = dateStringify('hh:mm:ss', source);
            expect(actual).toBe('13:14:15');
        })

        test('Test HH:mm:SS', () => {    
            const source = new Date('2020/05/11 13:14:15');
            const actual = dateStringify('HH:mm:SS', source);            
            expect(actual).toBe('13:14:15');
        })

        test('Test h:m:s', () => {    
            const source = new Date('2020/05/11 9:4:15');
            const actual = dateStringify('h:m:s', source);            
            expect(actual).toBe('9:4:15');
        })

        test('Test h:m:s', () => {    
            const source = new Date('2020/05/11 9:4:5');
            const actual = dateStringify('h:m:s', source);
            expect(actual).toBe('9:4:5');
        })

        test('Test H:m:S', () => {    
            const source = new Date('2020/05/11 9:4:15');
            const actual = dateStringify('H:m:S', source);
            expect(actual).toBe('9:4:15');
        })

        test('Test hh:mm:ss sss', () => {    
            const source = new Date(2020, 5, 11, 12, 40, 50, 888);
            const actual = dateStringify('hh:mm:ss sss', source);
            expect(actual).toBe('12:40:50 888');
        })

        test('Test hh:mm:ss ssss', () => {    
            const source = new Date(2020, 5, 11, 12, 40, 50, 888);
            const actual = dateStringify('hh:mm:ss ssss', source);
            expect(actual).toBe('12:40:50 0888');
        })
    })

    describe('utc time zone', () => {
        var source = Date.UTC(2020, 5, 11, 12, 40, 50, 888);
        test('Test hh:mm:ss', () => {
            const actual = dateStringify('hh:mm:ss', source, { utc: true });            
            expect(actual).toBe('12:40:50');
        })
    })

    describe('12-hour', () => {    
        test('Test 12:40:50 ', () => {
            const source = new Date(2020, 5, 11, 12, 40, 50, 888);
            const actual = dateStringify('hh:mm:ss', source, { use12hour: true });
            expect(actual).toBe('12:40:50');
        })

        test('Test hh:mm:ss 13:40:50', () => {
            const source = new Date(2020, 5, 11, 13, 40, 50, 888);
            const actual = dateStringify('hh:mm:ss', source, { use12hour: true });            
            expect(actual).toBe('01:40:50');
        })

        test('Test h:m:s 13:40:50', () => {
            const source = new Date(2020, 5, 11, 13, 40, 50, 888);
            const actual = dateStringify('h:m:s', source, { use12hour: true });
            expect(actual).toBe('1:40:50');
        })

        test('Test h:m:s 0:6:6', () => {
            const source = new Date(2020, 5, 11, 0, 6, 6, 888);
            const actual = dateStringify('h:m:s', source, { use12hour: true });            
            expect(actual).toBe('12:6:6');
        })
    })
})