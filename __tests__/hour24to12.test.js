const hour24to12 = require('../src/util/hour24to12');

// https://wenku.baidu.com/view/50f1c7af80eb6294dc886c5a.html
describe('hour24to12', () => {
    
    test('should return 12 when 24-hour is 0', () => {
        expect(hour24to12(0)).toBe(12);
    })

    test('should return 1 when 24-hour is 1', () => {
        expect(hour24to12(1)).toBe(1);
    })

    test('should return 12 when 24-hour is 12', () => {        
        expect(hour24to12(12)).toBe(12);
    })

    test('shoul return 2 when 24-hour is 14', () => {        
        expect(hour24to12(14)).toBe(2);
    })

    test('should return 11 when 24-hour is 23', () => {        
        expect(hour24to12(23)).toBe(11);
    })
})