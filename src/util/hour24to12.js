
module.exports = exports = function hour24to12(hour) {
    hour = hour % 12;
    if(hour === 0) {
        hour = 12;
    }
    return hour
}