/**
 * Module export
 */
module.exports = exports = prefixZeroNumber;

function prefixZeroNumber(origin, format, forcePrefix) {
    origin = origin + '';
    const originLength = origin.length;
    const formatLength = format.length;

    if(forcePrefix || formatLength > originLength) {
        origin = new Array(Math.max(formatLength, originLength) - originLength)
            .fill(0)
            .concat(origin)
            .join('');
        
        if(origin.length > formatLength) {
            origin = origin.substring(origin.length - formatLength);
        }
    }
    
    return origin;
}