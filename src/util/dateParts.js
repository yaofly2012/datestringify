const prefixZeroNumber = require('./prefixZeroNumber');
const hour24to12 = require('./hour24to12');

module.exports = exports = [
    {
        pattern: /^Y$/i,
        getVal: function(format, option) {
            const origin = option.utc ? this.getUTCFullYear() : this.getFullYear();
            return prefixZeroNumber(origin, format, true);
        }
    },
    {
        pattern: /^M$/,
        getVal: function(format, option) {
            const origin = (option.utc ? this.getUTCMonth() : this.getMonth()) + 1;
            return prefixZeroNumber(origin, format);
        }
    },
    {
        pattern: /^d$/i,
        getVal: function(format, option) {
            const origin = option.utc ? this.getUTCDate() : this.getDate();
            return prefixZeroNumber(origin, format);
        }
    },
    {
        pattern: /^H$/i,
        getVal: function(format, option) {
            let origin = option.utc ? this.getUTCHours() : this.getHours();
            if(option.use12hour) {
                origin = hour24to12(origin);
            }
            return prefixZeroNumber(origin, format);
        }
    },
    {
        pattern: /^m$/,
        getVal: function(format, option) {
            const origin = option.utc ? this.getUTCMinutes() : this.getMinutes();
            return prefixZeroNumber(origin, format);
        }
    },
    {
        pattern: /^S$/i, // second or milliSeconds
        getVal: function(format, option) {
            const origin = format.length > 2 
                ? this[option.utc ? 'getUTCMilliseconds' : 'getMilliseconds']() 
                : this[option.utc ? 'getUTCSeconds' : 'getSeconds']();
            
            return prefixZeroNumber(origin, format);
        }
    }
];