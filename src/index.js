const dateParts = require('./util/dateParts');
const toString = Object.prototype.toString;

function isString(target) {
    return toString.call(target) === '[object String]';
}

/**
 * Module export
 */
module.exports = exports = dateStringify;

const defautOption = {
    utc: false, // Indicate use UTC, Default zone is local
    use12hour: false
}

function dateStringify(format, date, option) {
    option = Object.assign({}, defautOption, option);
    let result = date;

    if(!isString(format) || !date) {
        return result;
    }   

    try {
        if(!(date instanceof Date)) {
            date = new Date(date);
        }

        result = format.replace(/([YyMmDdHhSs])\1*/g, (datePartFormat, $1) => {
            const datePart = dateParts.find(datePart => datePart.pattern.test($1));
            return datePart && datePart.getVal 
                ? datePart.getVal.call(date, datePartFormat, option) 
                : datePartFormat;
        });
    } catch(e) {}

    return result;
}