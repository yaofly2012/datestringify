# DateStringify
A minimal, practical date format library.

[![NPM Version](https://img.shields.io/npm/v/datestringify.svg)](https://www.npmjs.com/package/datestringify)
[![NPM Downloads](https://img.shields.io/npm/dm/datestringify.svg)](https://www.npmjs.com/package/datestringify)

# Installation
```js
$ npm install datestringify
```

# Usage
```js
const dateStringify = require('datestringify');

// Sat May 16 2020 13:45:30 GMT+0800
const date = new Date(2020, 4, 16, 13, 45, 30, 666);

// '2020-05-16'
console.log(dateStringify('yyyy-MM-dd', date))

// '2020/05/16'
console.log(dateStringify('yyyy/MM/dd', date))

// '2020-5-16'
console.log(dateStringify('yyyy-M-d', date))

// '2020-05-16 13:45:30'
console.log(dateStringify('yyyy-MM-dd HH:mm:ss', date))

// '2020-05-16 13:45:30 666'
console.log(dateStringify('yyyy-MM-dd HH:mm:ss sss', date))

// 12-hour '2020-05-16 01:45:30'
console.log(dateStringify('yyyy-MM-dd HH:mm:ss', date, { use12hour: true }))

// 12-hour '2020-05-16 1:45:30'
console.log(dateStringify('yyyy-MM-dd H:m:s', date, { use12hour: true }))
```

# Syntax
```js
dateStringify(formatString, date, option)
```
## [formatString](http://www.ecma-international.org/ecma-262/5.1/#sec-15.9.1.15)
Required, <`string`>

|Format|Description|
|--|--|
|`YYYY`, `yyyy`|The decimal digits of the year 0000 to 9999 in the Gregorian calendar|
|`YY`, `yy`|The decimal digits of the year 00 to 99 in the Gregorian calendar|
|`MM`| The month of the year from 01 (January) to 12 (December)|
|`M`| The month of the year from 1 (January) to 12 (December)|
|`DD`, `dd`| The day of the month from 01 to 31|
|`D`, `d`| The day of the month from 1 to 31|
|`HH`, `hh`| The number of complete hours that have passed since midnight as two decimal digits from 00 to 24|
|`H`, `h`| The number of complete hours that have passed since midnight as a decimal digits from 0 to 24|
|`mm`| The number of complete minutes since the start of the hour as two decimal digits from 00 to 59|
|`m`| The number of complete minutes since the start of the hour as a decimal digits from 0 to 59|
|`SS`, `ss`| The number of complete seconds since the start of the minute as two decimal digits from 00 to 59|
|`S`, `s`| The number of complete seconds since the start of the minute as a decimal digits from 0 to 59|
|`SSS`, `sss`| The number of complete milliseconds since the start of the second as three decimal digits from 000 to 999|

## date 
Required, [<`Date | timestamp | dateString`>](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date)

## option
Optional, and the default is:
```js
{
    utc: false, // true: use UTC, otherwise use local time zone
    use12hour: false
}
```

# Run UT
To run the test suite, first install the dependencies, then run `npm test`:
```js
$ npm install
$ npm test
```